import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'deq-fakedata',
  templateUrl: './fakedata.component.html',
  styleUrls: ['./fakedata.component.scss']
})
export class FakedataComponent implements OnInit {

  chartElements = [
    {type: 'line', title: 'line', subtitle: 'line data', style: 'col-xs-6',
      data: {
        datasets: [{
          data: [
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100)
          ]
        }],
        labels: [
          '2018-03-07 15:00',
          '2018-06-07 15:00',
          '2018-08-07 15:00',
          '2018-09-22 15:00',
          '2018-10-31 15:00'
        ]
      },
      options: {
        title: {
          text: 'Chart.js Time Scale'
        },
        scales: {
          xAxes: [{
            type: 'time',
            time: {
              parser: 'YYYY-MM-DD HH:mm',
              // round: 'day'
              tooltipFormat: 'll HH:mm'
            },
            scaleLabel: {
              display: false,
              labelString: 'Date'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: false,
              labelString: 'value'
            }
          }]
        },
      }}
    ];

  constructor() { }

  ngOnInit() {
  }

}
