import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'deq-reportspage',
  templateUrl: './reportspage.component.html',
  styleUrls: ['./reportspage.component.scss']
})
export class ReportspageComponent implements OnInit {

  reports;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.auth.getReports().subscribe(res => {
      this.reports = res.body.data;
    });
  }

  viewReport(id) {
    this.router.navigate([`reports/${id}`]);
  }

}
