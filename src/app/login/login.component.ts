import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'deq-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  formError = false;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {

    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {

  }

  logIn() {
    if (this.form.valid) {
      const val = this.form.value;

      this.auth.logIn(val.email, val.password)
        .subscribe(result => {
          // console.log('error status', result.status);
            console.log(result);
            this.formError = false;
            this.auth.isLogged = true;
            this.router.navigate(['home']);
        }, error => {
          if (error.status === 401) {
            this.auth.isLogged = false;
            // console.log('error logging', error);

            this.formError = true;
          }
        });
    }
  }

}
