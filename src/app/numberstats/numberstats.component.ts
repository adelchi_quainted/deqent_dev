import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'deq-numberstats',
  templateUrl: './numberstats.component.html',
  styleUrls: ['./numberstats.component.scss']
})
export class NumberstatsComponent implements OnInit {

  @Input() statNumber;
  @Input() title;
  @Input() subtitle;
  @Input() color;

  constructor() { }

  ngOnInit() {
  }

}
