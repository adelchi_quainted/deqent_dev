import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'deq-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form: FormGroup;


  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {




    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

  }

  signUp() {
    const val = this.form.value;
    if (this.form.valid) {
      // console.log(this.form.value);

      this.auth.signUp(val.firstName, val.lastName, val.email, val.password)
        .subscribe(result => {
          if (result) {

            // console.log(result);
            localStorage.setItem('userEmail', val.email);
            localStorage.setItem('userPwd', val.password);
            this.router.navigate(['confirmemail']);
          }
        });


    }
  }

  ngOnInit() {


  }

}
