import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'deq-temp',
  templateUrl: './temp.component.html',
  styleUrls: ['./temp.component.scss']
})
export class TempComponent implements OnInit {

  chartElements; /*= [
    {type: 'number', title: 'title 1', subtitle: 'sub 1', data: Math.round(Math.random() * 100), style: 'col-xs-3'},
    {type: 'number', title: 'title 2', subtitle: 'sub 2', data: Math.round(Math.random() * 100), style: 'col-xs-3'},
    {type: 'number', title: 'title 3', subtitle: 'sub 3', data: Math.round(Math.random() * 100), style: 'col-xs-3'},
    {type: 'number', title: 'title 4', subtitle: 'sub 4', data: Math.round(Math.random() * 100), style: 'col-xs-3'},
    {type: 'doughnut', title: 'doughnut', subtitle: 'doughnut data', style: 'col-xs-6'},
    {type: 'line', title: 'line', subtitle: 'line data', style: 'col-xs-6'},
    {type: 'bar', title: 'bar', subtitle: 'bar data', style: 'col-xs-6',
      data: {
        datasets: [{
          data: [
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100)
          ]
        }],
        labels: [
          'Red',
          'Orange',
          'Yellow',
          'Green',
          'Blue'
        ]
      }}
    ];*/

  constructor(private auth: AuthService) {

  }

  ngOnInit() {
    this.auth.getStats().subscribe(res => {
      this.chartElements = res.body.data;
    });
  }

}
