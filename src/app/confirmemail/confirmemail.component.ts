import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'deq-confirmemail',
  templateUrl: './confirmemail.component.html',
  styleUrls: ['./confirmemail.component.scss']
})
export class ConfirmemailComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService) {

    this.form = this.fb.group({
      codeConfirm : ['', Validators.required]
    });

  }

  ngOnInit() {
  }

  validateCode() {
    if (this.form.valid) {
      const val = this.form.value;
      const id = localStorage.getItem('userID');
      const email = localStorage.getItem('userEmail');

      this.auth.validateCode(id, email, val.codeConfirm).subscribe( res => {
        // console.log(res);
      });
    }

  }

}
