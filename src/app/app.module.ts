import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatInputModule, MatButtonModule, MatFormFieldModule, MatToolbarModule,
  MatCardModule, MatTableModule, MatPaginatorModule, MatMenuModule, MatIconModule
} from '@angular/material';
import { ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignupComponent } from './signup/signup.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginComponent } from './login/login.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfirmemailComponent } from './confirmemail/confirmemail.component';
import { ChartElementComponent } from './chart-element/chart-element.component';
import { TempComponent } from './temp/temp.component';
import { NumberstatsComponent } from './numberstats/numberstats.component';
import { FakedataComponent } from './fakedata/fakedata.component';
import { HyipListComponent } from './hyip-list/hyip-list.component';
import { TablestatsComponent } from './tablestats/tablestats.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { ReportDetailComponent } from './report-detail/report-detail.component';
import { ReportspageComponent } from './reportspage/reportspage.component';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    ToolbarComponent,
    DashboardComponent,
    ConfirmemailComponent,
    ChartElementComponent,
    TempComponent,
    NumberstatsComponent,
    FakedataComponent,
    HyipListComponent,
    TablestatsComponent,
    TransactionsComponent,
    ReportDetailComponent,
    ReportspageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
