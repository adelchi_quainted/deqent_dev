import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import * as moment from 'moment';


@Component({
  selector: 'deq-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private auth: AuthService) {
    this.auth.isUserLogged();
  }
}
