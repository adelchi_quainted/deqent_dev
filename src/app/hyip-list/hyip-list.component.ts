import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'deq-hyip-list',
  templateUrl: './hyip-list.component.html',
  styleUrls: ['./hyip-list.component.scss']
})
export class HyipListComponent implements OnInit {

  displayedColumns = ['preview', 'name', 'paying', 'amountInvested', 'time', 'stats'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private auth: AuthService) {


  }

  ngOnInit() {

    this.auth.getHyips().subscribe(res => {
        this.dataSource = new MatTableDataSource(res.body.data);

    },
        error => console.log(error),
      () => {

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      // console.log(this.dataSource.paginator);
      // console.log(this.dataSource.sort);

    });





  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
