import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { SignupComponent } from '../signup/signup.component';
import { LoginComponent } from '../login/login.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { IsloggedService } from '../services/islogged.service';
import { ConfirmemailComponent } from '../confirmemail/confirmemail.component';
import { TempComponent } from '../temp/temp.component';
import { FakedataComponent } from '../fakedata/fakedata.component';
import { HyipListComponent } from '../hyip-list/hyip-list.component';
import { ReportDetailComponent } from '../report-detail/report-detail.component';
import { ReportspageComponent } from '../reportspage/reportspage.component';

export const AppRoutes: Routes = [
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'confirmemail', component: ConfirmemailComponent},
  { path: 'hyips', component: HyipListComponent},
  { path: 'reports', component: ReportspageComponent},
  { path: 'reports/:id', component: ReportDetailComponent},

  { path: '', component: DashboardComponent, canActivate: [IsloggedService]},
  { path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
