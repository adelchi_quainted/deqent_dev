import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'deq-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  @Input() chartData;
  @Input() title;
  @Input() subtitle;

  KeysGetter = Object.keys;

  constructor() { }

  ngOnInit() {
  }

}
