import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'deq-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  initials;

  constructor(public auth: AuthService, private router: Router) {

    if (localStorage.getItem('initials')) {
      this.initials = localStorage.getItem('initials');
    }
  }

  ngOnInit() {
  }

  navigateReports() {
    this.router.navigate(['reports']);
  }

  navigateHome() {
    this.router.navigate(['']);
  }

}
