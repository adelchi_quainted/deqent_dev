import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs/index';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'deq-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.scss']
})
export class ReportDetailComponent implements OnInit, OnDestroy {

  getRouteParams: Subscription;
  report;

  keysGetter = Object.keys;

  constructor(private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.getRouteParams = this.route.params.subscribe(params => {
      if (params && params['id']) {
        this.getReport(params['id']);
      }
    });
  }

  ngOnDestroy() {

    this.getRouteParams.unsubscribe();
  }


  getReport(id) {

    this.auth.getReport(id).subscribe(res => {
      this.report = res.body.data;
      console.log(res);
    });

  }

}
