import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'deq-tablestats',
  templateUrl: './tablestats.component.html',
  styleUrls: ['./tablestats.component.scss']
})
export class TablestatsComponent implements OnInit {

  @Input() chartData;
  @Input() title;
  @Input() subtitle;

  displayedColumns: string[] = ['date', 'value', 'amount'];

  dataSource;




  constructor() { }

  ngOnInit() {

    this.dataSource = this.chartData;


    console.log(this.dataSource, this.chartData);
  }

}
