import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public token: string;
  isLogged: boolean;
  initials;


  constructor(private http: HttpClient, private router: Router) {

      // check user status
      this.isLogged = !!localStorage.getItem('userToken');
      this.initials = localStorage.getItem('initials');
  }

  isUserLogged() {

    this.isLogged = !!localStorage.getItem('userToken');

  }


  getStats(): Observable<any> {
    const options = new HttpHeaders({
      'x-access-token': localStorage.getItem('userToken')
    });

    return this.http.get(`${environment.API_SERVER}stats`, {headers: options, observe: 'response'});

  }

  getReports(): Observable<any> {
    const options = new HttpHeaders({
      'x-access-token': localStorage.getItem('userToken')
    });

    return this.http.get(`${environment.API_SERVER}reports`, {headers: options, observe: 'response'});

  }


  getReport(id): Observable<any> {
    const options = new HttpHeaders({
      'x-access-token': localStorage.getItem('userToken')
    });

    return this.http.get(`${environment.API_SERVER}reports/${id}`, {headers: options, observe: 'response'});
  }

  getHyips(): Observable<any> {
    const options = new HttpHeaders({
      'x-access-token': localStorage.getItem('userToken')
    });

    return this.http.get(`https://deqent.herokuapp.com/api/v1/hyips`, {headers: options, observe: 'response'});

  }

  signUp(firstName: string, lastName: string, email: string, password: string): Observable<any> {

    const options = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post(
      `${environment.API_SERVER}users`,
      JSON.stringify({ firstName, lastName, email, password}),
      {headers: options, observe: 'response'})
      .pipe(
        tap((response: HttpResponse<any>) => {

          const id = response.body.data.userID;
          localStorage.setItem('userID', id);
          // console.log(response);
          return response;
        }),

      catchError(this.handleError)
  );
  }


  logIn(email: string, password: string): Observable<any> {

    const options = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post(
      `${environment.API_SERVER}authenticate`,
      JSON.stringify({ email, password}),
      {headers: options, observe: 'response'})
      .pipe(
        tap((response: HttpResponse<any>) => {

          console.log('res data', response.body.data);

          this.initials = response.body.data.user.initials;
          localStorage.setItem('initials', response.body.data.user.initials);

          this.token = response.body.data.token;
          localStorage.setItem('userToken', this.token);
          // console.log(response);
          // this.isLogged = true;
          return response;
        })) /*,

      catchError(this.handleError))*/ ;
  }


  validateCode(id, email, code): Observable<any> {
    const options = new HttpHeaders({
      'Content-Type' : 'application/json'
    });

    return this.http.post(
      `${environment.API_SERVER}users/${id}/validate`,
      JSON.stringify({email, code}),
      {headers: options, observe: 'response'})
      .pipe(
        tap((response: HttpResponse<any>) => {
         return response;
      }),
        catchError(this.handleError));
  }

  logout() {
    localStorage.clear();
    this.isLogged = false;
    this.router.navigate(['login']);
  }

  private handleError(error: any) {

    console.log(error);

    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(error);
  }
}
