import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsloggedService implements CanActivate {

  constructor(private router: Router, private auth: AuthService) { }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (this.auth.isLogged) {
      return true;
    } else {

      this.router.navigate(['login']);
      return false;
    }
  }
}
