import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import * as moment from 'moment';

@Component({
  selector: 'deq-chart-element',
  templateUrl: './chart-element.component.html',
  styleUrls: ['./chart-element.component.scss']
})
export class ChartElementComponent implements OnInit {

  chart: Chart;
  @ViewChild('chart')
  htmlRef: ElementRef;
  @Input() chartType;
  @Input() chartData;
  @Input() chartOptions;
  @Input() title;
  @Input() subtitle;

  optionsChart;

  colors = ['#009688', '#4CAF50', '#FFC107', '#FF5722', '#607D8B', '#2196F3', '#3F51B5'];

  constructor() { }

  ngOnInit() {


    this.optionsChart = this.chartOptions || '';


    // console.log(this.chartData, this.chartOptions, this.optionsChart);

    this.chart = new Chart(this.htmlRef.nativeElement, {
      type: this.chartType,
      data: this.chartData,
      options: {
        // cutoutPercentage: 20,
        responsive: true,
        maintainAspectRatio: false,
        legend: this.optionsChart.legend || {display : false},
        title: this.optionsChart.title || '',
        scales: this.optionsChart.scales || ''
      }
    });
  }

}
