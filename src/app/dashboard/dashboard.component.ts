import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'deq-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  chartElements;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.auth.getStats().subscribe(res => {
      this.chartElements = res.body.data;
      console.log('all the data', res.body.data);
    });
  }

}
