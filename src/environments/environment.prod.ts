export const environment = {
  production: true,
  API_SERVER: `https://${window.location.host}/api/v1/`,
  SOCKET_SERVER: 'https://quainted.com/',
  APP_URL: 'deqent.herokuapp.com',
  PROTOCOL: 'https://',
  SUB: '',
  COOKIE_DOMAIN: '.quainted.com',
  IMGPATH: 'img',
  AMZURL: 'https://do28d100mt2b1.cloudfront.net'
};
